package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import proto.element.ElementProto.Element;
import proto.element.ElementProto.Element.Builder;

public class Main {

	AtomicInteger idGenerator = new AtomicInteger(1);
	public static void main(String[] args) {
		Main mainApp = new Main();
		
		final int depth = 1;
		
		Element.Builder elementBuilder = mainApp.getElementBuilder("element1-" + depth,  depth);
		
		Element imutableParent = elementBuilder.build();
		setParents(elementBuilder,imutableParent);
		
		Element element = elementBuilder.build();
		
		System.out.println("elem1 parent:");
		Element p1 = element.getChildren(0).getParent();
		System.out.println("p1 size:" + p1.getSerializedSize());
		System.out.println(p1.toString());
		
		System.out.println("elem2 parent:");
		Element p2 = element.getChildren(1).getParent();
		System.out.println("p2 size:" + p1.getSerializedSize());
		System.out.println(p2.toString());
		
		System.out.println("total serialized size: " + element.getSerializedSize());
		
	}
	
	
	
	private static void setParents(Element.Builder element,Element imutableParent) {
		
		for(int i=0; i< element.getChildrenBuilderList().size(); i++)
		{
			Element.Builder child = element.getChildrenBuilder(i);
			child.setParent(imutableParent);
		}
	}


	private Element.Builder getElementBuilder (final String elementName, final int depth )
	{
		if(depth >4) return null;
		
		final List<String> attributes = new ArrayList<>();
		for(int i=0;i< 100;i++)
		{
			attributes.add("attribute" + i);
		}
		
		final Element.Builder elementBuilder = Element.newBuilder();
		elementBuilder.setId(idGenerator.getAndIncrement());
		elementBuilder.setDepth(depth);
		elementBuilder.setName(elementName);
		
		//elementBuilder.addAllAttributes(attributes);
		System.out.println("adding children for current element: " + elementName);
		int newDepth= depth+1;
		for(int i = 0; i<2;i++)
		{
			final String childElementName = "element" + i + "-" + newDepth;
			System.out.println("\t\t childElementName: [" + i + "/" + newDepth +"] "  + childElementName);
			final Builder childElement = getElementBuilder(childElementName,newDepth);
			if(childElement!=null)
			{
				elementBuilder.addChildren(childElement);
			}
		}
		
		return elementBuilder;
	}

}
